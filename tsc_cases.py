import os
import pandas as pd
from helpers import load_to_bq_from_dataframe

# Local env to gcp authentication
gcp_user_key = os.path.join(os.path.dirname(os.path.abspath(__file__)), "cne-lam-jupnwops-lab-6447e26abc3b.json")
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = gcp_user_key

# Reading .csv file on a Pandas dataframe
df = pd.read_csv(
    "/users/jsanto01/PycharmProjects/cneApps/calls_volume/datasources/results/df_xls_calls_complete_class.csv")

df_e65 = pd.read_csv(
    "/users/jsanto01/PycharmProjects/cneApps/calls_volume/datasources/results/df_all_e65.csv")

df_t19 = pd.read_csv(
    "/users/jsanto01/PycharmProjects/cneApps/calls_volume/datasources/results/df_all_t19.csv")

df_y3 = pd.read_csv(
    "/users/jsanto01/PycharmProjects/cneApps/calls_volume/datasources/results/df_all_y3.csv")



# Defining a new column names (to English)
columns = ['Case id', 'Open Case Datetime', 'GW Name', 'Beam Id', 'Real Problem (PT-BR)',
           'Customer Type Request (PT-BR)', 'Real problem and Customer Request', 'Case Category']
df.columns = columns

# Converting the Date/Time column to a pandas datetime type
df['Open Case Datetime'] = pd.to_datetime(df['Open Case Datetime'], errors='coerce')

# Converting Beam Id column to numeric and integer
df['Beam Id'] = pd.to_numeric(df['Beam Id'], errors='coerce')

df_e65['BEAM'] = pd.to_numeric(df_e65['BEAM'], errors='coerce')
df_t19['BEAM'] = pd.to_numeric(df_t19['BEAM'], errors='coerce')
df_y3['BEAM'] = pd.to_numeric(df_y3['BEAM'], errors='coerce')

# Dropping rows with NA/null values
df.dropna(inplace=True)

df_e65.dropna(inplace=True)
df_t19.dropna(inplace=True)
df_y3.dropna(inplace=True)

# Converting Beam Id to int after dropping NAs
df['Beam Id'] = df['Beam Id'].astype('int')

df_e65['BEAM'] = df_e65['BEAM'].astype('int')
df_t19['BEAM'] = df_t19['BEAM'].astype('int')
df_y3['BEAM'] = df_y3['BEAM'].astype('int')

# Converting all object columns to category
columns_as_object = df.select_dtypes(['object']).columns
df[columns_as_object] = df[columns_as_object].astype('category')

columns_as_object_e65 = df_e65.select_dtypes(['object']).columns
df_e65[columns_as_object_e65] = df_e65[columns_as_object_e65].astype('category')
columns_as_object_t19 = df_t19.select_dtypes(['object']).columns
df_t19[columns_as_object_t19] = df_t19[columns_as_object_t19].astype('category')
columns_as_object_y3 = df_y3.select_dtypes(['object']).columns
df_y3[columns_as_object_y3] = df_y3[columns_as_object_y3].astype('category')

# Fixing the data replacing it for the correct gw name
df['GW Name'].replace('Hortolândia-SP', 'Hortolândia', inplace=True)

# Case Id as index of the dataframe
df.set_index('Case id', inplace=True)

# Sorting by Datetime
df.sort_values('Open Case Datetime', ascending=False, inplace=True)

# Table destination and google function to insert the table
table_id = "cne-lam-jupnwops-lab.tsc_cases_latam.tsc_cases_brazil"
load_to_bq_from_dataframe(df, table_id)

table_id_e65 = "cne-lam-jupnwops-lab.tsc_cases_latam.norm_cases_e65"
load_to_bq_from_dataframe(df_e65, table_id_e65)
table_id_t19 = "cne-lam-jupnwops-lab.tsc_cases_latam.norm_cases_t19"
load_to_bq_from_dataframe(df_t19, table_id_t19)
table_id_y3 = "cne-lam-jupnwops-lab.tsc_cases_latam.norm_cases_y3"
load_to_bq_from_dataframe(df_y3, table_id_y3)
from google.cloud import bigquery


def load_to_bq_from_dataframe(dataframe, table_id):
    """
    Load contents of a pandas DataFrame to a table.
    https://cloud.google.com/bigquery/docs/samples/bigquery-load-table-dataframe

    :param dataframe:
    :param table_id: table destination (project_id.dataset.table)
    :param job_config: configs of the job
    :return:
    """

    client = bigquery.Client()

    job_config = bigquery.LoadJobConfig(
        write_disposition="WRITE_TRUNCATE",
    )
    job = client.load_table_from_dataframe(dataframe, table_id, job_config=job_config)
    job.result()

    table = client.get_table(table_id)
    print(f"Loaded {table.num_rows} rows and {len(table.schema)} columns to {table_id}")